#! /bin/bash

stack_name="Services"
script_name=`basename "$0"`
_env_path=''
if [ -e .service/.env ]; then
    _env_path='.service/.env'
elif [ -e .env ]; then
    _env_path='.env'
fi

setEnv() {
    echo "Creating new .env file ⚙️"
    declare -A ENV_ARRAY

    echo ""
    echo "Service setup"

    page_root=${PWD##*/}
    # Service name
    read -e -p "$(tput sgr0)$(tput setaf 6)Site name (A-Z a-z 0-9 - _) (Default: ${page_root}): $(tput sgr0)" page_name
        if [ -z "$page_name" ]; then
            page_name="${page_root}"
        fi
    ENV_ARRAY[SERVICE_NAME]=$page_name
    ENV_ARRAY[COMPOSE_PROJECT_NAME]=$page_name

    # Service Domain
    read -e -p "$(tput sgr0)$(tput setaf 6)Site Domain (Default: ${page_name}.localhost): $(tput sgr0)" page_domain
        if [ -z "$page_domain" ]; then
            page_domain="${page_name}.localhost"
        fi
    ENV_ARRAY[SERVICE_DOMAIN]=$page_domain
    ENV_ARRAY[SERVICE_ROOT_DIR]=$page_root

    ENV_ARRAY[USER_ID]=1000

    echo ""
    echo "Database setup"

    # Database host
    read -e -p "$(tput sgr0)$(tput setaf 6)Database host (Default: db): $(tput sgr0)" db_host
    if [ -z "$db_host" ]; then
        db_host=db
    fi
    ENV_ARRAY[WORDPRESS_DB_HOST]=$db_host

    # Database user
    read -e -p "$(tput sgr0)$(tput setaf 6)Database user (Default: root): $(tput sgr0)" db_user
    if [ -z "$db_user" ]; then
        db_user=root
    fi
    ENV_ARRAY[WORDPRESS_DB_USER]=$db_user

    # Database password
    read -e -p "$(tput sgr0)$(tput setaf 6)Database password (Default: password): $(tput sgr0)" db_pass
    if [ -z "$db_pass" ]; then
        db_pass=password
    fi
    ENV_ARRAY[WORDPRESS_DB_PASSWORD]=$db_pass

    # Database name
    read -e -p "$(tput sgr0)$(tput setaf 6)Database name (Default: ${page_name}): $(tput sgr0)" db_name
    if [ -z "$db_name" ]; then
        db_name=${page_name}
    fi
    ENV_ARRAY[WORDPRESS_DB_NAME]=$db_name

    # Database prefix
    read -e -p "$(tput sgr0)$(tput setaf 6)Table prefix (Default: wp_): $(tput sgr0)" db_prefix
    if [ -z "$db_prefix" ]; then
        db_prefix=wp_
    fi
    ENV_ARRAY[WORDPRESS_TABLE_PREFIX]=$db_prefix

    echo ""
    echo "Page setup"

    # Wordpress debug
    read -e -p "$(tput sgr0)$(tput setaf 6)Debug mode in wordpress (0 | 1) (Default: 1): $(tput sgr0)" wp_debug
    if [ -z "$wp_debug" ]; then
        wp_debug=1
    fi
    ENV_ARRAY[WORDPRESS_DEBUG]=$wp_debug

    # Wordpress url (same as service domain)
    ENV_ARRAY[WORDPRESS_URL]=$page_domain

    # Wordpress title
    read -e -p "$(tput sgr0)$(tput setaf 6)Wordpress Title (Default: ${page_name}): $(tput sgr0)" wp_title
    if [ -z "$wp_title" ]; then
        wp_title=${page_name}
    fi
    ENV_ARRAY[WORDPRESS_TITLE]=$wp_title

    # Wordpress title
    read -e -p "$(tput sgr0)$(tput setaf 6)Wordpress Admin Email (Default: services@oakdigital.dk): $(tput sgr0)" wp_email
        if [ -z "$wp_email" ]; then
        wp_email='services@oakdigital.dk'
    fi
    ENV_ARRAY[WORDPRESS_ADMIN_EMAIL]=$wp_email

    # Wordpress username
    read -e -p "$(tput sgr0)$(tput setaf 6)Wordpress Admin Username (Default: oakdigital): $(tput sgr0)" wp_username
        if [ -z "$wp_username" ]; then
        wp_username='oakdigital'
    fi
    ENV_ARRAY[WORDPRESS_ADMIN_USERNAME]=$wp_username

    # Wordpress password
    read -e -p "$(tput sgr0)$(tput setaf 6)Wordpress Admin Password (Default: test1234): $(tput sgr0)" wp_password
        if [ -z "$wp_password" ]; then
        wp_password='test1234'
    fi
    ENV_ARRAY[WORDPRESS_ADMIN_PASSWORD]=$wp_password


    echo ""
    echo "Env file"
    read -e -p "$(tput sgr0)$(tput setaf 6).env file path (Default: ./.service/.env): $(tput sgr0)" _env_path
    if [ -z "$_env_path" ]; then
        _env_path='./.service/.env'
    fi

    ENV_ARRAY[POST_INSTALL_CMD]=""
    ENV_ARRAY[ENV_PATH]=`realpath --relative-to=.service/ ${_env_path}`
    touch ${_env_path}

    for i in "${!ENV_ARRAY[@]}"; do
        printf "%s=\"%s\"\n" "$i" "${ENV_ARRAY[$i]}" >> ${_env_path}
    done
}

startServer() {
    if [ -z "$_env_path" ]; then
        echo "$(tput sgr0)$(tput setaf 6)No env found, please run $(tput sgr0)${script_name} --env $(tput sgr0)$(tput setaf 6)to set up a new one$(tput sgr0)"
    else
        docker-compose \
            --file .service/docker-compose.yaml \
            --env-file `realpath --relative-to=.service/ ${_env_path}` \
            #--project-name "${stack_name}" \
            up -d

        source ${_env_path}

        postInstall
    fi
}

stopServer() {
    docker-compose \
        --file .service/docker-compose.yaml \
        down
}

downloadBoilerplate() {
    echo "$(tput sgr0)$(tput setaf 6)Downloading boilerplate from bitbucket; make sure your ssh key is uploaded$(tput sgr0)"

    mkdir temp
    git clone git@bitbucket.org:oakdigital/wp-timber-boilerplate.git temp

    rm -f temp/install.sh
    rm -f temp/.gitignore
    rm -f temp/README.md

    cp -r temp/* .
    rm -rf temp
}

usage() {
    echo "Hello world!"
}

postInstall() {
    source ${_env_path}
    docker_id=`docker ps -aqf "name=${SERVICE_NAME}"`

    # Pre installed plugins
    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Downloading plugins$(tput sgr0)"
    docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
        wp plugin install imagify 'https://connect.advancedcustomfields.com/index.php?a=download&p=pro&k=b3JkZXJfaWQ9OTc1NjB8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE3LTAxLTE3IDE3OjIyOjQy'
    #docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
    #    wp plugin install

    # Activate theme
    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Activate boilerplate$(tput sgr0)"
    docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
        wp theme activate boilerplate

    # Activate acf
    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Activating ACF$(tput sgr0)"
    docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
        wp plugin activate advanced-custom-fields-pro

    # Run composer
    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Running composer install$(tput sgr0)"
    docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
        composer install

    # Run npm
    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Running npm install$(tput sgr0)"
    docker exec -w "/var/www/${SERVICE_ROOT_DIR}/" ${docker_id}\
        npm i

    echo ""
    echo "$(tput sgr0)$(tput setaf 6)Removing extras$(tput sgr0)"
    rm -rf wp-content/themes/twentynineteen
    rm -rf wp-content/themes/twentytwenty
    rm -rf wp-content/themes/twentytwentyone

    rm -rf wp-content/plugins/akismet
    rm -f wp-content/plugins/hello.php
}

guide() {
    echo "$(tput sgr0)$(tput setaf 6)Hi! My name is Malthe 🧑‍💻 $(tput sgr0)"
    echo "$(tput sgr0)$(tput setaf 6)I will now help you set up a new local wordpress page!$(tput sgr0)"
    echo "$(tput sgr0)$(tput setaf 6)First we need to check a few things (just press enter to continue)$(tput sgr0)"
    echo ""
    read -e -p "$(tput sgr0)$(tput setaf 6)Is Docker 🐳 installed? $(tput sgr0)- If not, go to the readme for more information."

    if [[ $OSTYPE == "msys" ]]; then
        read -e -p "$(tput sgr0)$(tput setaf 6)Windows 🪟 detected! Are you using WSL? $(tput sgr0)- If not, go to the readme for more information."
    fi

    read -e -p "$(tput sgr0)$(tput setaf 6)Is your workspace 📁 setup correctly? $(tput sgr0)- If not, go to the readme for more information."
    echo ""

    if [ -z "$_env_path" ]; then
        echo "$(tput sgr0)$(tput setaf 6)No .env file found $(tput sgr0)let's make one"
        setEnv
    else
        echo "$(tput sgr0)$(tput setaf 6)Using .env file found at$(tput sgr0) ${_env_path}"
    fi

    read -r -p "$(tput sgr0)$(tput setaf 6)Download boilerplate 🌳? [y/N]$(tput sgr0) " response
    case "$response" in
        [yY][eE][sS]|[yY])
            downloadBoilerplate
            ;;
        *)
            ;;
    esac

    read -r -p "$(tput sgr0)$(tput setaf 6)Start server now? [Y/n]$(tput sgr0) " response

    case "$response" in
        [nN][oO]|[nN])
            echo "$(tput sgr0)$(tput setaf 6)You can start the server later by running$(tput sgr0) ${script_name} --start"
            ;;
        *)
            startServer

            echo ""
            echo "$(tput sgr0)$(tput setaf 6)🎉 All finished! 🎉$(tput sgr0)"
            echo "$(tput sgr0)$(tput setaf 6)You can find you new site at:$(tput sgr0) https://${SERVICE_DOMAIN}"
            ;;
    esac
}


if [[ $1 == '--start' || $1 == '-u' ]]; then
    startServer
elif [[ $1 == '--stop' || $1 == '-s' ]]; then
    stopServer
elif [[ $1 == '--env' || $1 == '-e' ]]; then
    setEnv
elif [[ $1 == '--boilerplate' || $1 == '-b' ]]; then
    downloadBoilerplate
elif [[ $1 == '--help' || $1 == '-h' ]]; then
    usage
else
    guide
fi